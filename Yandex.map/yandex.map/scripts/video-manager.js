/**
 * Управлять прозрачностью слайдера, оставшегося уровнем ниже
 */
function switchSliderOpacity(transparecy){
    var opa=(transparecy)? '0.2':'1';
    $(['images-container','object-data']).each(function(){
        document.getElementById(this).style.opacity=opa;
    });
}