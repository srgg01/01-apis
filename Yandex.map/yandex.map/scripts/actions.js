$(function(){
    // показать/спрятать список
    var manageListBlock = $('.list_manager'),
        listPoint = document.querySelectorAll('div#menu-container ol li');
    // показать/скрыть список
    $('>div',manageListBlock).on('click', function(){
        $(manageListBlock).toggle(200); //console.dir($(manageListBlock));
        $('#menu-list').slideToggle(300);
    });
    // обработать клик по элементу списка
    $(listPoint).on('click', function(){
        var closer;
        if(closer=getBalloonCloser()){
            $(closer).trigger('click'); //console.log('Go close!');
        }
        var markerIndex = $(this).index();
        loadSliderBox(markerIndex);
    });
    // обработать закрытие балуна
    /*$('div.mapArea').on('click', function(){
        console.log(getBalloonCloser());


    });*/
    // обработать наведение курсора на элемент списка
    $(listPoint).on('mouseover mouseleave', function(event){
        var rollover=(event.type=='mouseover');
        myPlacemarks[$(this).index()].options.set('iconImageHref', getMarksBgImg(rollover));
    });
    // закрыть превью
    $(getPreviewContainer()).on('click', '.close', function(){
        var pNode =  $(this).parent(),
            videoBoxId = 'content-video';
        if($(pNode).attr('id')==videoBoxId){
            $(pNode).fadeOut(200, function() {
                $(pNode).remove();
                switchSliderOpacity();
            });
        }else{
            if(!document.getElementById(videoBoxId)){
                $(pNode).fadeOut(200, function(){
                    $(getShell()).fadeOut(200);
                    $(getLoadedContentContainer()).html('');
                });
            }
        }
    });
    // скорректировать позицию превью на изменение размера окна
    $(window).on('resize', managePreview);
});