// Как только будет загружен API и готов DOM, выполняем инициализацию
ymaps.ready(init);

var myMaps = {}, myPlacemarks = [];

function init() {
    var Map, Markers,
        mapsParams = {
            map1: {
                center: [55.76, 37.64],
                markers: [
                    [55.779393, 38.651318],  // Павлов Посад
                    [55.383874, 36.724225],  // Наро-Фоминск
                    [54.744659, 35.242463]   // Юхнов
                ],
                images:{
                    inBalloon: [
                        ["hangar_noginsk.jpg","Ангар прямоугольный","г. Павловский Пасад"],
                        ["hangar_naro-fominsk.jpg","Ангар арочный","г. Наро-Фоминск"],
                        ["hangar_youhnow.jpg","Ангар полусферический","г. Юхнов"]
                    ]
                },
                objectsData:[
                    // площадь, год постройки, колич. картинок
                    [602,2009,5],
                    [200,2003,5],
                    [819,1954,5],
                ]
            }
        };
    for (var map in mapsParams) {
        //
        Map = mapsParams[map];
        myMaps[map] = new ymaps.Map(map, {
            center: Map.center,//Петрозаводск //[55.76, 37.64], // Москва
            zoom: 7,
            type: 'yandex#map'
        });
        // добавим маркеры
        Markers = Map.markers;
        for (var i = 0, j = Markers.length; i < j; i++) {
            var pMark = new ymaps.Placemark(
                Markers[i], {
                    iconContent: '<div class="marker">'+(i+1)+'</div>',
                    balloonContent: '<img class="preview" onclick="loadSliderBox(' + i + ')" src="assets/images/thumbnails/' + Map.images.inBalloon[i][0] + '" />' +
                     '<div class="balloon-pic-signature">' + Map.images.inBalloon[i][1] + '</div>' +
                     '<div>' + Map.images.inBalloon[i][2] + '</div>' +
                     '<ul>' +
                        '<li>'+Map.objectsData[i][0]+'м.</li>' +
                        '<li>'+Map.objectsData[i][1]+'г.</li>' +
                        '<li onclick="loadSliderBox(' + i + ')">'+Map.objectsData[i][2]+'</li>' +
                        '<li><button onclick="loadVideoBox(' + (i + 1) + ')"> Видео</button></li>' +
                     '</ul>'
                },
                {
                    iconImageHref: getMarksBgImg(),
                    iconImageSize: [39, 44],
                    iconImageOffset: [-20, -20],
                    // Определим интерактивную область над картинкой.
                    iconShape: {
                        type: 'Circle',
                        coordinates: [0, 0],
                        radius: 20
                    }
                }
            );
            pMark.events.add('mouseenter', function (e) {
                e.get('target').options.set({iconImageHref: getMarksBgImg(true)});
            });
            pMark.events.add('mouseleave', function (e) {
                e.get('target').options.set({iconImageHref: getMarksBgImg()});
            });
            // обработать цвета балунов по клику одного из них
            pMark.events.add('click', function (e) {
                dropBalloonsColors(i);
            });
            //console.dir(pMark);
            myPlacemarks.push(pMark);
            myMaps[map].geoObjects.add(pMark);
        }
        // добвить элементы управления картой:
        // тип карты
        myMaps[map].controls.add(
            new ymaps.control.TypeSelector(
                ['yandex#map', 'yandex#hybrid', 'yandex#satellite']
            )
        );
        // масштаб
        myMaps[map].controls.add('zoomControl', {
            float: 'none',
            position: {
                right: 40,
                top: 75
            }
        });
    }
}